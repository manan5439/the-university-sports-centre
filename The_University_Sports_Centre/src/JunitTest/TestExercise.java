package JunitTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import the_university_sports_centre.DataRecords;
import the_university_sports_centre.TimeTable;



//import the_university_sports_centre.*;

public class TestExercise {

	@Test
	 public void testForDataRecord() {
		DataRecords dr = new DataRecords();
		//test for checking, is our time table has 48 day classes or not? 
	      assertEquals(48, dr.getTimeTable(),0);
	    //test for checking is time table by date is working as expected. check either timetable by date provide valid week ends or not
			assertTrue( dr.TimetableByDate("22-3"));
			
			//test for checking is time table by name is working as expected. check either expected timetable by valid exercise or not
			assertTrue( dr.TimetableByName("yoga"));
			
	      
	   }
	@Test
	 public void testForTimetableClass() {
		TimeTable tt = new TimeTable(1,"yoga",1.50,"00","1-3","9:00to11:00");
		//make sure every exercise have maximum 4 vacancy   
		assertTrue(tt.increaseVacancy()==false);
		assertTrue(tt.decreaseVacancy());
		assertTrue(tt.decreaseVacancy());
		assertTrue(tt.decreaseVacancy());
		assertTrue(tt.decreaseVacancy());
		//make sure every exercise have minimum 0 vacancy  
		assertTrue(tt.decreaseVacancy()==false);
		//check after decreasing vacancy by four time, number of student must be 4  
		assertEquals(4, tt.getNumberOfStudentPerExercise(),0);
		
			
	      
	   }


}
