package the_university_sports_centre;

public class Exercise {
	private int exercise_id;
	private String exercise_name;
	private double price;
	protected String date ="";
	private double totalIncome=0 ;
	private int rating =0;
	 Exercise(int exercise_id,String name,double price){ 
		this.exercise_id = exercise_id;
		this.exercise_name=name;
		this.price = price;
	}
	public int getId() {
		return exercise_id;
	}
	public String getName() {
		return exercise_name;
	}
	public String getDate() {
		return this.date;
	}
	public double getPrice() {
		return price;
	}
	public double getTotalIncome() {
		return this.totalIncome;
	}
	
	public void setTotalIncome(double totalIncome) {
		this.totalIncome =totalIncome;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	
	
	public int getRating() {
		return this.rating;
	}
}
