package  the_university_sports_centre;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Scanner;

public class DataRecords {
	private ArrayList<TimeTable> timetable_list = new ArrayList<TimeTable>();
	private ArrayList<Booking> booking_lists = new ArrayList<Booking>(); 
	private Exercise yoga = new Exercise(1,"yoga",1.50);
    private Exercise zumba = new Exercise(2,"zumba",2.00);
    private Exercise aquacise = new Exercise(3,"Aquacise",5);
    private Exercise box_fit = new Exercise(4,"Box Fit",3.5);
    private Exercise body_blitz = new Exercise(5,"Body Blitz",3.75);
    private Exercise sport = new Exercise(6,"sport",1.00);
    public Exercise [] exerciseClass = {yoga,zumba,aquacise,box_fit,body_blitz,sport};
    
    Scanner scan = new Scanner(System.in);
    
    // constructor for intitializing Data record, wich generete time table by given input. here our timetable will generate from march and allocate every weekend slot upto 8 weeks
    public DataRecords() {
 	   
 	   int year = 2020;
 	   int month = Calendar.MARCH;
 	   Calendar cal = new GregorianCalendar(year, month, 1);
 	   int i =0;
 	   
 	  
 	   
 	  //use 16 because every week there is one sunday and saturday and every day 3 class. 8X2=16X3=48 class.
          while(i<16){
         	    // get the day of the week for the current day
   	       int day = cal.get(Calendar.DAY_OF_WEEK);
   	       // check if it is a Saturday or Sunday
   	       if (day == Calendar.SATURDAY || day == Calendar.SUNDAY) {
   	    	String date = cal.get(Calendar.DAY_OF_MONTH)+"-"+ (cal.get(Calendar.MONTH)+1);
   	    	for(int j =0;j<3;j++) {
   	    		//if saturday then allocate first 3 class else last 3 class
   	    		int exerciseClassIndex=day == Calendar.SATURDAY?j:j+3;
   	    		//create timetable object and add in to array lists, which cntain 48 lactures 
   	    	    TimeTable timetable = new TimeTable (exerciseClass[exerciseClassIndex].getId(),exerciseClass[exerciseClassIndex].getName(),exerciseClass[exerciseClassIndex].getPrice(),i+""+j,date, j==0?"9:00to11:00":j==1?"14:00to116:00":"18:00to20:00");
   	          timetable_list.add(timetable);
   	    		
   	    	}
         
          i++;
   	       }
   	       // advance to the next day
   	       cal.add(Calendar.DAY_OF_YEAR, 1);
   	       
 }
     }
    
    //this function for find timetable by date
     public boolean TimetableByDate(String date ) {
 	   
 	   Iterator<TimeTable> itr=timetable_list.iterator();
 	   boolean found = false;
 	  System.out.println("Slot Id "+"Date"+"   "+"Classes"+"    "+"Slot"+"    "+"Vacancy");
 	   while(itr.hasNext()){
 		   TimeTable st=(TimeTable)itr.next();
// 		   System.out.println(date+" "+st.getDate());
 		   if(st.getDate().equals(date)) {
 			   found = true;
 			   System.out.println(st.getSlotId()+"        "+st.getDate()+"  "+st.getName()
 	           +"    "+st.getSlot()+
 	           "      "+st.getVacancy()); 
 		   }
 		 
 	   }
 	   if(!found) {
 		   System.out.println("sorry there are not any slot available for given Date or you provided wrong input formate try again by using (d-m) formate");
 		   return false;
 	   }
 	   return true;
 	   
    }
    
     //this function for find timetable by name
    public boolean TimetableByName(String name ) {
 	   
 	   Iterator<TimeTable> itr=timetable_list.iterator();
 	   boolean found = false;
 	  System.out.println("Slot Id "+"Date"+"  "+"Classes"+"   "+"Slot"+"    "+"Vacancy");
 	   while(itr.hasNext()){
 		   TimeTable st=(TimeTable)itr.next();
// 		   System.out.println(date+" "+st.getDate());
 		   if(st.getName().equals(name)) {
 			   found = true;
 			   System.out.println(st.getSlotId()+"        "+st.getDate()+"  "+st.getName()
 	           +"  "+st.getSlot()+
 	           "      "+st.getVacancy()); 
 		   }
 		 
 	   }
 	   if(!found) {
 		   System.out.println("sorry there are not any slot available for given name");
 		   return false;
 	   }
 	  return true;
    }
    
    //this is for full timetable
    public int getTimeTable() {
 	   Iterator<TimeTable> itr=timetable_list.iterator();  
 	   System.out.println("Slot Id "+"Date"+"  "+"Classes"+"   "+"Slot"+"     "+"Vacancy");
// 	   for(int i=0;i<timetable_list.size();i++) {
// 		   
// 	   }
 	   while(itr.hasNext()){  
            TimeTable st=(TimeTable)itr.next();
            
            System.out.println(st.getSlotId()+"        "+st.getDate()+"  "+st.getName()
            +"  "+st.getSlot()+
            "      "+st.getVacancy());  
            
        } 
 	  return timetable_list.size();
    }
    // this function for booking slot
  public void bookingSlot(String name,String slotid) {
 	   
 	   for(int i=0;i<booking_lists.size();i++) {
 		  if(booking_lists.get(i).getBookedSlotId().equals(slotid)&&booking_lists.get(i).getStudentName().equals(name)) {
 			  System.out.println("you already booked given slot,please select other slot, if you want!");  
 			  return;
 		  }
 		  
 	   }
 	   
 	   
 	   Iterator<TimeTable> itr=timetable_list.iterator();
 	   boolean found = false;
 	   while(itr.hasNext()){
 		   TimeTable st=(TimeTable)itr.next();
// 		   System.out.println(date+" "+st.getDate());
 		   if(st.getSlotId().equals(slotid)) {
 			   if(st.getVacancy()>0) {
 				   found = true;
 				   Booking booking = new Booking(st.getId(),st.getName(),st.getPrice(),st.getSlotId(),name,st.getDate());
 				   booking_lists.add(booking);
 				   st.decreaseVacancy();
 				  exerciseClass[st.getId()-1].setTotalIncome(exerciseClass[st.getId()-1].getTotalIncome()+exerciseClass[st.getId()-1].getPrice());
 				   System.out.println("your booking has been done");
 				  
 			   }
 			   else {
 				   System.out.println("This class is full please try different class");
 				   return;
 			   } 
 		}
 		 
 	   }
 	   if(!found) {
 		   System.out.println("provided slot id is wrong. please try again");
 	   }
 	   
 	   
    }
  
  // this function for changing allocated slot
  public void changeSlot(String name) {
	  boolean founded = false;
 	 if(booking_lists.size()==0) {
 		 System.out.println("name is not found");
 		  return;
 	 }
 	 System.out.println("Student Name     Slot Id      Date");
 	 for(int i=0;i<booking_lists.size();i++) {
 		  if(booking_lists.get(i).getStudentName().equals(name)) {
 			  founded = true;
 			  for(int j=0;j<timetable_list.size();j++) {
 				 if(timetable_list.get(j).getSlotId().equals(booking_lists.get(i).getBookedSlotId())) {
 					 System.out.println(booking_lists.get(i).getStudentName()+"              "+timetable_list.get(j).getSlotId()+"      "+timetable_list.get(j).getDate());
 				 }
 				  
 			   }
 		  }
// 		  else {
// 			  System.out.println("name is not found");
// 			  return;
// 		  }
 		  
 	   }
 	 if(!founded) {
 		  System.out.println("name is not found");
			  return; 
 	 }
 	 
 	 System.out.println();
 	 System.out.println("Here is your all alocated slot,please enter slotId which you have to change");
 	 String old_slot_id = scan.next();
 	 System.out.println("please enter a new SlotId");
 	 String new_slot_id = scan.next();
 	  founded = false;
 	  
 	 for(int i=0;i<booking_lists.size();i++) {
 		  if(booking_lists.get(i).getStudentName().equals(name)&&booking_lists.get(i).getBookedSlotId().equals(old_slot_id)) {
 			  for(int j=0;j<timetable_list.size();j++) {
 				  if(timetable_list.get(j).getSlotId().equals(old_slot_id)) {
 					 founded = true;
 					  timetable_list.get(j).increaseVacancy();
 					  
 					  bookingSlot(name, new_slot_id);
 					 exerciseClass[timetable_list.get(j).getId()-1].setTotalIncome(exerciseClass[timetable_list.get(j).getId()-1].getTotalIncome()-exerciseClass[timetable_list.get(j).getId()-1].getPrice());
 					  booking_lists.remove(i);
 				  }
 				 
 				  
 			   }
 		  }
// 		  else {
// 			  System.out.println("student name and slot id is not match, please try again");
// 		  }
 		  
 	   }
 	 if(!founded) {
 		  System.out.println("student name and slot id is not match, please try again"); 
 	 }
 	 
 	 
 	 
  }
  //this function for rating. 
  public void Rating(String name) {
 	 if(booking_lists.size()==0) {
 		 System.out.println("name is not found");
 		  return;
 	 }
 	 System.out.println("Student Name     Slot Id      Date");
 	 boolean isFound = true;
 	 for(int i=0;i<booking_lists.size();i++) {
 		  if(booking_lists.get(i).getStudentName().equals(name)) {
 			  isFound = false;
// 			  for(int j=0;j<timetable_list.size();j++) {
// 				 if(timetable_list.get(j).getSlotId().equals(booking_lists.get(i).getBookedSlotId())) {
 					 System.out.println(booking_lists.get(i).getStudentName()+"              "+booking_lists.get(i).getBookedSlotId()+"          "+booking_lists.get(i).getDate());
// 				 }
// 				  
// 			   }
 		  }
 		
 		  
 	   }
 	  if(isFound) {
 		  System.out.println("name is not found");
 		  return;
 	  }
 	 
 	 System.out.println("which class would you like to rate, enter SlotId");
 	 String slot_id = scan.next();
 	 System.out.println("give rating from ranging from 1 to 5 (1: Very dissatisfied, 2: Dissatisfied, 3: Ok, 4: Satisfied, 5: Very Satisfied");
 	 
 	
 		 try {
 			 int rating = scan.nextInt();
 			 if(rating>0&&rating<6) {
 				 for(int i=0;i<booking_lists.size();i++) {
 				 if(booking_lists.get(i).getBookedSlotId().equals(slot_id)&&booking_lists.get(i).getStudentName().equals(name)) {
 					 booking_lists.get(i).setRating(rating);
 					 System.out.println("you have successfully rated to "+slot_id+" slot id");
 				 }
 				}
 			 }
 			 else {
 				 System.out.println("something wrong. make sure you provided rating from 1 to 5, and try again ");
 				 Rating(name);
 			 }
 		 } catch (InputMismatchException e) {
 		     System.out.println("something wrong. make sure you provided rating from 1 to 5, and try again ");
 		     Rating(name);
 		 }
 	

 	 
 	 
  }
  //this function for generating report for student per class and rating
  private String generateReportForStudentAndRating(int index){
 	 int count = 0;
 	 double rating = 0;
 	 for(int i=0;i<booking_lists.size();i++) {
 		 if(timetable_list.get(index).getSlotId().equals(booking_lists.get(i).getBookedSlotId())) {
 			 count ++;
 			 rating += booking_lists.get(i).getRating();
// 			 System.out.println(booking_lists.get(i).getRating()+" "+booking_lists.get(i).getStudentName());
 		 }
 	 }
 	 
 	 double avg = count>0? rating/count:0;
 	 return timetable_list.get(index).getSlotId()+"  "+timetable_list.get(index).getDate()+"  "+
 			 timetable_list.get(index).getName()
      +"  "+timetable_list.get(index).getSlot()+
      "    "+timetable_list.get(index).getNumberOfStudentPerExercise()
      +"  "+(avg);
  }
  
//  public void generateReportForIncome(){	 
//// 	 double array [] = {0.0,0.0,0.0,0.0,0.0,0.0};
//// 	 for(int i=0;i<booking_lists.size();i++) {
// 		 
//// 		 if(timetable_list.get(index).getSlotId().equals(booking_lists.get(i).getBookedSlotId())) {
//// 		double array [booking_lists.get(i).getId()-1];
//// 				 exerciseClass[booking_lists.get(i).getId()-1].setTotalIncome(exerciseClass[booking_lists.get(i).getId()-1].getTotalIncome()+exerciseClass[booking_lists.get(i).getId()-1].getPrice());
//
//// 		 }
//// 	 }
//
//	 
// 	 
// 	 
// 	 
//  }
  
  
  // this function is for printing higrevenue
 public void printIncomeWithHighRevenue() {
	 double maxIncome = 0;
		String maxIncomeExersiseName = "";
		 for(Exercise ex : exerciseClass)
		 {
			System.out.println("Total Income for "+ex.getName()+" is "+ex.getTotalIncome()); 
			if(maxIncome<ex.getTotalIncome()) {
				maxIncome = ex.getTotalIncome();
				maxIncomeExersiseName = ex.getName();
			}
		 }
		System.out.println("A "+maxIncomeExersiseName+"  has generated a higest income, which is "+maxIncome); 
  }
  
// this function use for generate report handle which use in main class
  public void generateReport(String select){	
	  
	  switch(select) {
	  case "1":
		  System.out.println("Here is your generated Report for 1st 4 weeks "); 
		  System.out.println("Slot Id "+"Date"+"  "+"Classes"+"   "+"Slot"+"   "+"Vacancy");
		  System.out.println("-------------------------------------------------------------");
	 		for(int i=0;i<timetable_list.size()/2;i++) {
	 				System.out.println(generateReportForStudentAndRating(i));
	 		}
		  break;
	  case "2":
		  System.out.println("Here is your generated Report for last 4 weeks "); 
		  System.out.println("Slot Id "+"Date"+"  "+"Classes"+"   "+"Slot"+"   "+"Vacancy");
		  System.out.println("-------------------------------------------------------------");
	 		for(int i=timetable_list.size()/2;i<timetable_list.size();i++) {
				System.out.println(generateReportForStudentAndRating(i));
		}
		  break;
	  case "3":
		  printIncomeWithHighRevenue();
		  break;
		 default:
			System.out.println("please enter valid input"); 
			 break;
			  
		  
	  }
// 	if(select.equals("1")) {
// 		
// 		 if(reportSelect.equals("2")){
// 			
// 			printIncomeWithHighRevenue();
//			}
// 		
// 	}else if(select.equals("2")) {
// 		System.out.println("Here is your generated Report for last 4 weeks "); 
// 		for(int i=timetable_list.size()/2;i<timetable_list.size();i++) {
// 			if(reportSelect.equals("1")) {
// 				System.out.println(generateReportForStudentAndRating(i));
// 			}
// 			else if(reportSelect.equals("2")){
// 				generateReportForIncome(i);
// 			}
// 		}
// 		 if(reportSelect.equals("2")){
// 			printIncomeWithHighRevenue();
//			}
// 		}
 	}
  }
    
    

