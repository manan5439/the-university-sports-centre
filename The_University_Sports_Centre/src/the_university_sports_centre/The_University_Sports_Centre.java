/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package the_university_sports_centre;
import java.util.*;

/**
 *
 * @author md19acb
 */
public class The_University_Sports_Centre {

    private static Scanner scan;

	/**
     * @param args the command line arguments
     */
   
 
 
    public static void main(String[] args) {
        // TODO code application logic here
    	
    	scan = new Scanner(System.in);
    	DataRecords dr = new DataRecords();
      boolean exit = false;
      String select = "" ;
      String student_name_find;
     
      while(!exit) {
    	  System.out.println("-----------------------------------------");
    	  System.out.println("Please Select Options");
          System.out.println("1. get full timetable");
            System.out.println("2. find timetable by date ");
              System.out.println("3. find timetable by exercise name ");
                System.out.println("4. book your class by id");
                  System.out.println("5. change your class ");
                  	System.out.println("6. give rating ");
                  		System.out.println("7. Report ");
                  			System.out.println("8. exit ");
                  				System.out.println("-----------------------------------------");
                      select =  scan.next();
    	  switch(select) {
    	  case "1":
    		  dr.getTimeTable();
    		  break;
    	  case"2":
    		  System.out.println("Enter date for searching timetable as (d-m) format. your classes will start from March 2020");
    		  String date = scan.next();
    		  System.out.println("-----------------------------------------");
    		  dr.TimetableByDate(date);
    		  
    		  break;
    	  case"3":
    		  System.out.println("Enter name for searching classes. We are providing following classes ");
    		  for(int i=0;i<6;i++) {
    			  System.out.println(i+"."+dr.exerciseClass[i].getName());
    		  }
    		  System.out.println("-----------------------------------------");
    		  String name = scan.next();
    		 
    		  dr.TimetableByName(name);
    		  System.out.println("-----------------------------------------");
    		  
    		  break;
    	  case"4":
    		  System.out.println("Enter your Name");
    		  String student_name = scan.next();
    		  System.out.println("Enter SlotId");
    		  String slot_id = scan.next();
    		  dr.bookingSlot(student_name,slot_id);
    		  System.out.println("-----------------------------------------");
    		  
    		  break;
    	  case"5":
    		  System.out.println("Enter your Name for finding your alocated slot");
    		  student_name_find = scan.next();
    		  dr.changeSlot(student_name_find);
    		  System.out.println("-----------------------------------------");
    		  break;
    	  case"6":
    		  System.out.println("Give Rating to the class");
    		  System.out.println("Enter your name so we can find your attended class");
    		   student_name_find = scan.next();
    		  dr.Rating(student_name_find);
    		  System.out.println("-----------------------------------------");
    		  break;
    	  case"7":
    		  System.out.println("1. Report for 1st 4 weeks about student and rating calculating");
    		  System.out.println("2. Report for 2nd 4 weeks about student and rating calculating");
    		  System.out.println("3. report for income calculate");
    		  System.out.println("-----------------------------------------");
    		  
    		  select = scan.next();
    		  
    		  dr.generateReport(select);	
    		  
    		  
//    		  String report_select = "";
//    		  switch(select) {
//    		  case "1":
//    			  System.out.println("1. report for student and rating calculating");
//    			  System.out.println("2. report for income calculate");
//    			  System.out.println("-----------------------------------------");
//    			  report_select = scan.next();
//    			  
//    			  break;
//    		  case "2":
//    			  System.out.println("1. report for student and rating calculating");
//    			  System.out.println("2. report for income calculate");
//    			  System.out.println("-----------------------------------------");
//    			  report_select = scan.next();
//    			  break;
//    		  case "3":
//    			  System.out.println("1. report for student and rating calculating");
//    			  System.out.println("2. report for income calculate");
//    			  System.out.println("-----------------------------------------");
//    			  report_select = scan.next();
//    			  break;
//    		  default:
//    			System.out.println("please enter valid input");
//    				  break;
//    				    
//    			  
//    		  }
    		  
    		  
    		  
    		  System.out.println("-----------------------------------------");
    		  break;
    	  case "8":
    		  System.out.println("Terminating.....");
    		  exit = true;
    		  break;
    		default:
    			System.out.println("Somthing wrong please try again. ");
    			System.out.println("-----------------------------------------");
    			break;
    	  
    	  }
      }
 
       
    }
    
}
