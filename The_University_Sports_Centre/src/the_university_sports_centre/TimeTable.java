package the_university_sports_centre;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author md19acb
 */
public class TimeTable extends Exercise {
	
	private String slotId = "";
	private String slot = "";
	
    private int vacancy = 4;
   
     public TimeTable(int exercise_id,String name,double price,String id,String date,String slot){
		super(exercise_id,name,price);
		this.slotId=id;
		this.slot = slot;
        this.date = date;
		
	}
	public String getSlotId() {
		return this.slotId;
	}
	public String getSlot() {
		return this.slot;
	}
	public int getVacancy() {
		return this.vacancy;
	}
	public boolean decreaseVacancy() {
		if(this.vacancy>0) {
			this.vacancy--;
			return true;
		}
		else 
			return false;
		
	}
	public boolean increaseVacancy() {
		if(this.vacancy<4) {
			this.vacancy++;
			return true;
		}
		else 
			return false;
	}
	
	public int getNumberOfStudentPerExercise() {
		return 4-this.vacancy;
	}
//private String [] exersiseClass = {"Yoga","Zumba","Aquacise","Box Fit","Body Blitz","sport"};





}
